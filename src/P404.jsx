import React from "react";

const p404 = () => (
  <>
    <br />
    <h3>404 página no encontrada</h3>
  </>
);

export default p404;
