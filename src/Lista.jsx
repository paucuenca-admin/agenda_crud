import React, {useContext} from "react";
import TraductorContext from "./TraductorContext.js";

import { Table } from "reactstrap";
import { Link } from "react-router-dom";

const Lista = (props) => {
  const Traductor = useContext(TraductorContext);
  //para crear las filas hacemos un "map" de los contactos recibidos
  let filas = props.contactos
    .map((contacto) => {
      return (
        <tr key={contacto.id}>
          <td>{contacto.id}</td>
          <td>{contacto.nombre}</td>
          <td>{contacto.email}</td>
          <td>
            <Link  to={"/modifica/" + contacto.id}><i className="fa fa-edit fa-2x"></i></Link>
          </td>
          <td>
            <Link  to={"/elimina/" + contacto.id}><i style={{color: "red"}} className="fa fa-trash fa-2x "></i></Link>
          </td>
        </tr>
      );
    });

  return (
    <>
    <br />
    <h3>{Traductor.traduce("contactos")}</h3>
    <Table>
      <thead>
        <tr>
          <th>#</th>
          <th>Nombre</th>
          <th>Email</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>{filas}</tbody>
    </Table>
    <br />
    <Link className="btn btn-success" to="/nuevo">{Traductor.traduce("nuevo")}</Link>
    </>
  );
};

export default Lista;
